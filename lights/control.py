# -*- coding: utf-8 -*-
import time

from lists.colors import colors 
from sense_hat import SenseHat

sense = SenseHat()
sense.rotation = 180
sense.low_light = False


def light (color):
    sense.clear(color)

def flash (r,g,b, pause=.5):    
    sense.clear(r,g,b)
    sense.low_light = False
    time.sleep(.05)
    sense.low_light = True
    time.sleep(.05)
    sense.clear()
    time.sleep(pause)


def buzz (r,g,b):
    for i in range(1,10):
        flash(r,g,b, pause = .05)


def notification (r,g,b):
    for i in range(1,5):
        flash(r,g,b)

def color (color):

    color = color.lower()
    rvb = (0,0,0)

    # On récupère le code RVB
    for c in enumerate(colors):
        #print(c[1][0])
        if c[1][0] == color:
            rvb = c[1][1]

    print ("Eclairage en %s : %s", color, rvb) 
    light(rvb)
    time.sleep(5)
    sense.clear()
